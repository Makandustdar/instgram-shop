from django import template

register = template.Library()


@register.simple_tag()
def number_format(num):
    return "{:,.0f}".format(float(num))
