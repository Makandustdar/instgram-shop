def configure(request):
    from configure.models import SiteConfiguration
    return {'configure': SiteConfiguration.get_solo()}
