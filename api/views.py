from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from provider.api.stormlikes import STORMLIKES


class UserMediaAPIView(APIView):
    def get(self, request, pk):
        provider = STORMLIKES()
        max_id = request.GET.get('max_id')
        pk_id = request.GET.get('pk')
        if max_id and pk:
            return Response(provider.get_more_media(pk, max_id, pk_id))
        else:
            return Response(provider.get_media(pk))
