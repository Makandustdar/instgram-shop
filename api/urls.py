from django.urls import path

from api.views import UserMediaAPIView

app_name = 'api'
urlpatterns = [
    path('user/<str:pk>/media/', UserMediaAPIView.as_view(), name='media'),
]
