from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=64)


class Package(models.Model):
    name = models.CharField(max_length=64)
    like_count = models.IntegerField(null=True,blank=True)
    fallow_count = models.IntegerField(null=True,blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    price = models.FloatField()
    discount = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
