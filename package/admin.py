from django.contrib import admin

from package.models import Category, Package


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    pass
