from django.db import models
from solo.models import SingletonModel


class SiteConfiguration(SingletonModel):
    name = models.CharField(max_length=255, default='Site Name')
    description = models.CharField(max_length=255, default='Site description')
    url = models.URLField(null=True, blank=True)
    logo = models.FileField(upload_to='images', null=True, blank=True)
    small_logo = models.FileField(upload_to='images', null=True, blank=True)

    maintenance_mode = models.BooleanField(default=False)

    STORMLINKS_XSRF_TOKEN = models.TextField(null=True, blank=True)
    STORMLINKS_SESSION = models.TextField(null=True, blank=True)
    STORMLINKS_TOKEN = models.TextField(null=True, blank=True)

    RECAPTCHA_PUBLIC_KEY = models.TextField(null=True, blank=True)
    RECAPTCHA_PRIVATE_KEY = models.TextField(null=True, blank=True)

    JAP_KEY = models.TextField(null=True, blank=True)
    MICROKS_KEY = models.TextField(null=True, blank=True)
    FLW2_KEY = models.TextField(null=True, blank=True)
    RTIMA_KEY = models.TextField(null=True, blank=True)
    INSTAMARKET24_KEY = models.TextField(null=True, blank=True)

    def __str__(self):
        return "Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"
