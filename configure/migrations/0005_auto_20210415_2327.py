# Generated by Django 3.1.7 on 2021-04-16 06:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configure', '0004_siteconfiguration_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteconfiguration',
            name='logo',
            field=models.FileField(blank=True, null=True, upload_to='images'),
        ),
    ]
