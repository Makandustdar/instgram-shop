# Generated by Django 3.2 on 2021-05-06 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configure', '0007_alter_siteconfiguration_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='FLW2_KEY',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='INSTAMARKET24_KEY',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='JAP_KEY',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='MICROKS_KEY',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='RTIMA_KEY',
            field=models.TextField(blank=True, null=True),
        ),
    ]
