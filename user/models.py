from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser,
    PermissionsMixin)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self,mobile, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            mobile=mobile
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    # accounts.password = password # bad - do not do this

    def create_superuser(self, mobile, email, password=None):
        user = self.create_user(
            email=email, mobile=mobile, password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    mobile = models.CharField(
        max_length=12,
        unique=True,
        verbose_name=_("Mobile number"),
    )
    email = models.EmailField(
        unique=True,
        null=True,
        blank=True
    )

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    published = models.DateTimeField(
        default=timezone.now,
        verbose_name=_('published')
    )
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    recovery = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['email']

    class Meta:
        permissions = (
            ("can_see_admin_page", "Can see admin page"),
        )
        ordering = ('-published',)

    def __str__(self):
        return "{}".format(self.mobile)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

