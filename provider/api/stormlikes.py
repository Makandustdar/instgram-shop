import json
import re

import requests
from bs4 import BeautifulSoup

from configure.models import SiteConfiguration


class STORMLIKES:
    def __init__(self):
        self.configure = SiteConfiguration.get_solo()

        self.__XSRF_TOKEN = self.configure.STORMLINKS_XSRF_TOKEN
        self.__SESSION = self.configure.STORMLINKS_SESSION
        self.__TOKEN = self.configure.STORMLINKS_TOKEN

    def __regex_operation_for_token(self, token):
        string = '"'
        for i in token:
            if i != '\n' and i != ' ' and i != ';':
                string += i
        pat = r'.*?\{token:(.*),lang*'
        match = re.search(pat, string)
        string_token = match.group(1)
        return (string_token)

    def __get_new_authentication_data(self):
        self.request = requests.get("https://stormlikes.com/package/50")
        self.soup = BeautifulSoup(self.request.content, 'html.parser')
        set_cookie = self.request.headers.get('Set-Cookie').split(';')
        self.__XSRF_TOKEN = set_cookie[0].split('=')[1]
        self.__SESSION = set_cookie[4].split('=')[1]
        self.__TOKEN = self.__get_request_token()
        return 1

    def __get_request_token(self):
        token_result = None
        for i in range(21):
            token = self.soup.find_all('script')[i].string
            if token:
                match = re.search(r'\btoken\b', str(token))
                if match:
                    token_result = self.__regex_operation_for_token(token)
        if token_result:
            return token_result.replace('"', '')
        return token_result

    def get_media(self, username):
        response = requests.request(
            "POST",
            "https://stormlikes.com/user/{}/media".format(
                username
            ),
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
                'Accept': '*/*',
                'Accept-Language': 'en-US,en;q=0.5',
                'content-type': 'application/json',
                'Connection': 'keep-alive',
                'Cookie': 'XSRF-TOKEN={}; stormlikes_session={};'.format(
                    self.__XSRF_TOKEN,
                    self.__SESSION
                ),
            },
            data=json.dumps({
                "_token": self.__TOKEN
            })
        )
        if response.status_code == 200:
            response = json.loads(response.text)
            if 'success' in response:
                return response
            else:
                return {
                    'success': False,
                    'message': "request unsuccess"
                }
        else:
            return {
                'status': False,
                'message': "provider not online!"
            }

    def get_more_media(self, username, max_id, pk):
        response = requests.request(
            "POST",
            "https://stormlikes.com/user/{}/media?max_id={}&pk={}".format(
                username,
                max_id,
                pk
            ),
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
                'Accept': '*/*',
                'Accept-Language': 'en-US,en;q=0.5',
                'content-type': 'application/json',
                'Connection': 'keep-alive',
                'Cookie': 'XSRF-TOKEN={}; stormlikes_session={};'.format(
                    self.__XSRF_TOKEN,
                    self.__SESSION
                ),
            },
            data=json.dumps({
                "_token": self.__TOKEN
            })
        )
        if response.status_code == 200:
            response = json.loads(response.text)
            if 'success' in response:
                return response
            else:
                return {
                    'success': False,
                    'message': "request unsuccess"
                }
        else:
            return {
                'status': False,
                'message': "provider not online!"
            }
