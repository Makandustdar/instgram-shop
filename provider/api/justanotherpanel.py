import json

import requests


# from configure.models import SiteConfiguration


class JAP:
    def __init__(self):
        # self.__api_key = SiteConfiguration.get_solo().JAP_KEY
        self.__api_key = "3fb9284f866610ed4c42355dfa81a89f"
        self.__base_url = "https://justanotherpanel.com/api/v2"

    def services(self):
        return self.__send_request({'action': 'services'})

    def add_order(self, service, link, quantity, runs=None, interval=None):
        return self.__send_request({
            'action': 'add',
            'service': service,
            'link': link,
            'quantity': quantity,
            'runs': runs,
            'interval': interval,
        })

    def status_order(self, order):
        return self.__send_request({
            'action': 'status',
            'order': order,
        })

    def balance(self):
        return self.__send_request({
            'action': 'balance',
        })

    def __send_request(self, payload):
        payload['key'] = self.__api_key
        response = requests.request("POST", self.__base_url, data=payload)
        return json.loads(response.text)
